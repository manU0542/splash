import React, { Component } from 'react';
import { Text, View,Image,StyleSheet ,MaskedViewIOS, Animated} from 'react-native';
export default class hbSplash extends Component {

  state={
    loadingProgress:new Animated.Value(0),
    animationDone:false
  }
  componentDidMount(){
    Animated.timing(this.state.loadingProgress,{
    toValue:1300,
    duration:900,
    useNativeDriver:true,delay:400}).start(()=>{this.setState({animationDone:true})});
  }
  render() {
    
    console.disableYellowBox = true; 
    const colorLayer=this.state.animationDone?null:<View style={[StyleSheet.absoluteFill,{backgroundColor:'lightblue'}]}></View>
    const whiteLayer=this.state.animationDone?null:<View style={[StyleSheet.absoluteFill,{backgroundColor:'white'}]}></View>
    
    const imageScale={
       transform:[
         {
           scale:this.state.loadingProgress.interpolate({
               inputRange: [0,1598],
               outputRange: [1,0],
            })
         }
       ]
     };
     const opacity = {
      transform: [
        {
          scale: this.state.loadingProgress.interpolate({
            inputRange: [-10, -5, 330],
            outputRange: [-49, 0, 0.3],
          }),
        },
      ],
    };
     
    return (
      <View style={{ flex: 1}}>


         <MaskedViewIOS  style={{width:'100%',height:'100%',borderColor:'#fff'}} >
            
            <Animated.View style={[styles.container,imageScale]}>
            <Image 
             source={require('./src/imgs/logo.png')}
            style={{width:"700%",height:"700%"}}
             />
            </Animated.View>
            </MaskedViewIOS>

         <MaskedViewIOS  style={{width:'100%',height:'100%',borderColor:'#fff',position:'absolute'}}>
         <Animated.View style={[styles.logoDesign,opacity]}>
         <Image 
           source={require('./src/imgs/hblogo.png')}
          style={{width:242,height:130,marginTop:3,marginLeft:3}}
           />
        </Animated.View>
        </MaskedViewIOS>

        <MaskedViewIOS  style={{width:'100%',height:'100%',position:'absolute',borderColor:'#fff'}}>
         <Animated.View style={[styles.Text,opacity]}>
         
         <Text style={{fontWeight:'bold',color:'#f2f2f2',fontSize:25}}>hogarBaber</Text>
    <View style={{flexDirection:'row',alignItems:'center',marginTop:'1%'}}><Text style={{fontSize:10,color:'#e8aeb7'}}>relax<Text style={{color:'#fff',fontSize:10}}>||</Text></Text><Text style={{fontSize:10,color:'#1c448e'}}>renew</Text><Text style={{color:'#fff',fontSize:10}}>||</Text><Text style={{fontSize:10,color:'#61e294'}}>revive</Text></View>
        </Animated.View>
        </MaskedViewIOS>
            
            </View>
            
                );
              }
}
const styles=StyleSheet.create({
  logoDesign:{
    height:'100%',
    width:'100%',
    alignItems:'center',
    justifyContent:'center'
  },
  container:{
    flex:1,
    
    alignItems:'center',
    justifyContent:'center'
  }
  ,Text:{
    alignItems:'center',marginTop:'135%'
  }

});